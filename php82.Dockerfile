#################
#### PHP 8.2 ####
#################

FROM php:8.2-buster AS php82
RUN apt clean
RUN apt update
ADD ./scripts /scripts
ADD ./bin/slack /usr/local/bin
RUN /scripts/php_install.sh
RUN apt install jq rsync -yqq

#################
#### Node 16 ####
#################

FROM php82 AS php82-node16
RUN /scripts/node16_install.sh
# Install autoreconf to fix gulp-imagin issue
RUN apt install dh-autoreconf -yqq

#################
#### Node 20 ####
#################

FROM php82 AS php82-node20
RUN /scripts/node20_install.sh
# Install autoreconf to fix gulp-imagin issue
RUN apt install dh-autoreconf -yqq

################
#### Python  ###
################

FROM php82-node16 AS php82-python
RUN apt install jq build-essential python-dev libreadline-dev libbz2-dev libssl-dev libsqlite3-dev libffi-dev -yqq
RUN curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
RUN echo "export PATH=\"/root/.pyenv/bin:\$PATH\"" >> ~/.bashrc
RUN . ~/.bashrc && pyenv install 3.7.2
ENV PATH="/root/.pyenv/versions/3.7.2/bin:$PATH"
RUN pip install pipenv
RUN apt clean

#########################
#### Dependency Scan  ###
#########################

FROM php82-python AS php82-node16-dependency-scan
ARG DEPENDENCY_SCAN_DEPLOY_TOKEN_USER
ARG DEPENDENCY_SCAN_DEPLOY_TOKEN_VALUE
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LC_CTYPE=en_US.UTF-8
WORKDIR /script
RUN /scripts/dependency-scan.sh
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony
CMD [ "pipenv", "run", "python", "./dependency-scan/main.py" ]

###########################
#### Elastic Beanstalk ####
###########################

FROM php82-python AS php82-eb
RUN git clone https://github.com/aws/aws-elastic-beanstalk-cli-setup.git
RUN python ./aws-elastic-beanstalk-cli-setup/scripts/ebcli_installer.py
ENV PATH="/root/.ebcli-virtual-env/executables:$PATH"
RUN apt clean
